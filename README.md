**PHP debugger 3.0** is a debugging tool suitable for production environment debugging and logging. Remote RPC logging is provided. Authentication by IP aqnd credentials.

##Getting started

Deze pagina bevat php-code die een aantal variabelen, arrays en een object debugt

Ga naar /debug om in te loggen,
schakel daarna de flags [DEBUG] , [TEST] en [LOG****] buttons in en reload de Debug Testpagina 1 of 2.

Open dan /debug/dump in een ander venster om de dump-resultaten te bekijken
of open de logging pagina /debug/logs om de logging te bekijken





De code die de testobjecten genereert :



```
#!php

class MyClass {
    public $var1 = 'value 1';
    public $var2 = 'value 2';
    public $var3 = 'value 3';
    public $var4 = array(1,2,3,4,5);


    protected $protected = 'protected aaldert';
    private   $private   = 'private aaldert';

    function iterateVisible() {
       echo "MyClass::iterateVisible:\n";
       foreach($this as $key => $value) {
           print "$key => $value\n";

       }
    }
}

$class = new MyClass();




$test=true;
$naam='aaldert';
$timestamp=time();

$leeg=NULL;

$test_array = array('$test'=>$test,'$naam'=>$naam,'$timestamp'=>$timestamp, '$var'=>23,'$array'=>array(1,3,5,7,9));








De debugger-code :



debug::I();//init

debug::V($naam,'$naam');
debug::V($test_array,'een test array');

debug::D(array('$test'=>$test,'$naam'=>$naam,'$timestamp'=>$timestamp,

 '$leeg'=>$leeg,'$class'=>$class),'array', 'array van testvariabelen');


debug::D($leeg,'leeg');
debug::D($class,'class');

debug::SES(__FILE__.__LINE__);//session 



debug::REQ(__FILE__.__LINE__);//request vars
debug::SER(__FILE__.__LINE__);//server
debug::GLB(__FILE__.__LINE__);//globals


debug::log('initial message');


if(TEST){
	$message="<h1 style=\"color:grey;border:1px solid grey;padding:4px;\">&nbsp;Testmodus ; ingelogd</h1>";
    debug::info('testmode is on');
} else {
	$message="";
    debug::info('testmode is off');
}

debug::warn('watch out, this is a warning message. Shows only if LOGWARN is true');
debug::error('an error message for you, shows only if LOGERROR is true');
```