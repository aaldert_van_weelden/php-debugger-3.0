<?php
error_reporting(E_ALL);

if (!isset($_SESSION)) session_start();

define('DIR',0);//diepte van de debug-folder in de root, AANPASSEN INDIEN NODIG
//TO DO : hier DIR automatisch bepalen
//TODO : put paths in separate include file
include(debug::getpath(DIR).'debug/config.php');


$host=debug::getpath(DIR,true);
define('HOST', $host);//webhost

if(isset($_COOKIE["PHPSESSID"])){
	define('DEBUGPATH',debug::getpath(DIR)."debug/temp/{$_COOKIE["PHPSESSID"]}_debug.txt");
	define('FLAGPATH',debug::getpath(DIR)."debug/temp/{$_COOKIE["PHPSESSID"]}_code.txt");
}
define('DEFAULTPATH',debug::getpath(DIR)."debug/dump/_code.txt");
//define("LOGGERPATH", debug::getpath(DIR) . 'debug/temp/log.txt');
define("LOCAL_STORAGE_DIR", debug::getpath(DIR) . 'debug/temp/');
define("LOG_FILE_NAME", 'log.txt');// The name of the log file

require_once('logger.php');

//wannee ingelogd en juiste ip-adres:
if( (isset($_SESSION['debug']) && $_SESSION['debug'] == 'open') && in_array($_SERVER['REMOTE_ADDR'],$a_IP) ){
	debug::makeCode();
	require(FLAGPATH);
	
} else {
	debug::makeCode(true);//alle flags op false zetten
	require(DEFAULTPATH);
}


class debug{

	public static function D($p_sVar,$p_sVarname='',$p_sBericht='') {//dump variabele
		
			if(DEBUG){
				$time		= date("Y-m-d : H-i-s ");
				$bericht	= $p_sBericht;
				$var		= $p_sVar;
				$varname	= $p_sVarname;
				
				$html=self::printArray($var, true, $varname, $bericht);
				
				$logfile = fopen(DEBUGPATH, "a");
				fputs($logfile, $html);
				fclose($logfile);
			} else {
				return false;
			}
			return $logfile;
	}
	
	public static function I($p_sBericht=''){
	
		if(DEBUG){
			//initieer de inhoud van de textfile vanuit de test-omgeving
			$time=date("Y-m-dTH:i:s");
			$_SESSION['debug_id']=self::get_debugID();
			$bericht='<p><i>Initiatie op '.$time.'&nbsp;->&nbsp;&nbsp;:&nbsp;'.(self::get_sender()).' : '.$p_sBericht.'</i></p><br><hr>'."\n";
			
			$logfile = fopen(DEBUGPATH, "w");
			fputs($logfile, $bericht);
			fclose($logfile);
			
		} else {
			return false;
		}
		return $logfile;
	}
	
	public static function Init($p_sBericht=''){
	
			//initieer de inhoud van de textfile vanuit de dump-view
			$time=date("Y-m-dTH:i:s");
			$sender  = self::get_sender();
			$bericht='<p><i>Initiatie op '.$time.'&nbsp;->&nbsp;'.$sender.'&nbsp;:&nbsp;'.$p_sBericht.'</i></p><br><hr>'."\n";
			
			$logfile = fopen(DEBUGPATH, "w");
			fputs($logfile, $bericht);
			fclose($logfile);
			return $logfile;
		
	}
    
    public static function InitLogger($p_sBericht=''){
    
    //initieer de inhoud van de textfile vanuit de logger-view
			$time=date("Y-m-dTH:i:s ");
			$sender  = self::get_sender();
			$bericht="Logging start&nbsp;".$time."&nbsp;".$sender."&nbsp;:&nbsp;".$p_sBericht."<br><br>";
			
			$logfile = fopen(LOCAL_STORAGE_DIR.LOG_FILE_NAME, "w");
			fputs($logfile, $bericht);
			fclose($logfile);
			return $logfile;
	}
	
	public static function GLB($p_sBericht='') {//vardump globals
		
			if(DEBUG){
				$time		= date("Y-m-dTH:i:s");
				$bericht	= $p_sBericht;
				$txt = self::vardump($GLOBALS);
                $lines = count(explode(chr(13),$txt));
                $height = $lines*100;
                $string = '<table style="width:100%">';
	   
                $time=date("Y-m-d : H-i-s ");
                $sender  = self::get_sender();
                $string .=	'<tr class="varheader"><th class="header_expanded leftpadding"><b>Varname</b></th><th style="background:none;">$GLOBALS&nbsp;:&nbsp;type = <i>all types</i></th></tr>'."\n";
                $string .=	"<tr><th><b>Time</b></th><th>$time</th></tr>\n";
                $string .=	"<tr><th><b>Sender</b></th><th>$sender</th></tr>\n";
                
                $string .= "<tr><td colspan=\"2\">";
	    
				$string .='<div class="var_dump">'.$txt.'</div>';
                $string .= "</td></tr>\n";
                $string .= '</table>';
				
				$logfile = fopen(DEBUGPATH, "a");
				fputs($logfile, $string);
				fclose($logfile);
			} else {
				return false;
			}
			return $logfile;
	}
	
	public static function V($p_sVar,$p_sBericht='') {//vardump
		
			if(DEBUG){
				$time	= date("Y-m-dTH:i:s");
				$sender = self::get_sender();
				$message = $p_sBericht;
				
				$string  = '<table style="width:100%">'; 
                $string .= '<tr class="varheader"><th class="header_expanded leftpadding"><b>Varname</b></th><th style="background:none;">Var Dump&nbsp;:&nbsp;type = <i>'.gettype($p_sVar).'</i></th></tr>'."\n";
                $string .=	"<tr><th><b>Time</b></th><th>$time</th></tr>\n";
                $string .=	"<tr><th><b>Sender</b></th><th>$sender</th></tr>\n";
				$string .=	"<tr><th><b>Message</b></th><th>$message</th></tr>\n";
				$string .=  "<tr><td colspan=\"2\">";
				$string .=self::vardump($p_sVar);
				$string .= "</td></tr>\n";
                $string .= '</table>';
				
				$logfile = fopen(DEBUGPATH, "a");
				fputs($logfile, $string);
				fclose($logfile);
			} else {
				return false;
			}
			return $logfile;
	}
	
	public static function SES($p_sBericht='') {//vardump session
		
			if(DEBUG){
				$time		= date("Y-m-dTH:i:s");
				$bericht	= $p_sBericht;
				$var		= $_SESSION;
				$varname	= '$_SESSION';
				
				$html=self::printArray($var, true, $varname, $bericht);
				
				$logfile = fopen(DEBUGPATH, "a");
				fputs($logfile, $html);
				fclose($logfile);
			} else {
				return false;
			}
			return $logfile;
	}
	
	public static function SER($p_sBericht='') {//vardump server
		
			if(DEBUG){
				$time		= date("Y-m-dTH:i:s");
				$bericht	= $p_sBericht;
				$var		= $_SERVER;
				$varname	= '$_SERVER';
				
				$html=self::printArray($var, true, $varname, $bericht);
				
				$logfile = fopen(DEBUGPATH, "a");
				fputs($logfile, $html);
				fclose($logfile);
			} else {
				return false;
			}
			return $logfile;
	}
	
	public static function REQ($p_sBericht=''){//vardump requestvariabelen
		
		if(DEBUG){
				$time		= date("Y-m-dTH:i:s");
				$bericht	= $p_sBericht;
				
				$html='';
				$html.=self::printArray($_GET, true, '$_GET', $bericht);
				$html.=self::printArray($_POST, true, '$_POST', $bericht);
				$html.=self::printArray($_COOKIE, true, '$_COOKIE', $bericht);
				
				$logfile = fopen(DEBUGPATH, "a");
				fputs($logfile, $html);
				fclose($logfile);
			} else {
				return false;
			}
			return $logfile;
	}
    
    //LOGGER
    public static function log($message=''){
        if(LOGDEBUG){
            $log = Logger::getInstance(); 
            $log->debug($message);
            return true;
        }
        else{
            return false;
        }
    }
    
    public static function info($message=''){
        if(LOGINFO){
            $log = Logger::getInstance();
            $log->info($message);
            return true;
        }
        else{
            return false;
        }
    }
    
    public static function warn($message=''){
       if(LOGWARN){
            $log = Logger::getInstance();
            $log->warn($message);
            return true;
        }
        else{
            return false;
        }
    }
    
    public static function error($message=''){
        if(LOGERROR){
            $log = Logger::getInstance();
            $log->error($message);
            return true;
        }
        else{
            return false;
        }
    }
		
	
	
	public static function makeCode($init=false){
		global $aFlags;
		global $_SESSION;
		$c='';
		$c.="<?php \n\n";
		foreach($aFlags as $name){
			$NAME=strtoupper($name);
			if(isset($_SESSION['debug_'.$name]) && $_SESSION['debug_'.$name] && !$init){
				$c.="\t define('$NAME', true);";
			} else {
				$c.="\t define('$NAME', false);";
			}
			
		}
		$c.="\n ?>";
		$init ? $txtfile = fopen(DEFAULTPATH, "w"):$txtfile = fopen(FLAGPATH, "w");
		fputs($txtfile, $c);
		fclose($txtfile);
		return $txtfile;	
		
	}
	
	public static function getPath ($p_iIndex=0, $p_iSelf=false){
		
		$s_path='';
		$a_path=explode("/", $_SERVER['PHP_SELF']);
		
		if($p_iIndex>count($a_path)) {
			$p_iIndex=count($a_path);	
		}
		
		for($t=0;$t<=$p_iIndex;$t++) {
			$t<$p_iIndex?$slash='/':$slash='';
			$s_path .= $a_path[$t].$slash;
	
		}
		
		if($p_iSelf){
			$path=$_SERVER['HTTP_HOST'].$s_path.'/';
			$path=str_replace("//", "/", $path);
			$path='http://'.$path;
		}
		else{
			$path=$_SERVER['DOCUMENT_ROOT'].$s_path.'/';
			$path=str_replace("//", "/", $path);
		}

	
	return $path;
	}
	
	
	public static function printArray($var, $title = true, $varname='', $message='') {
		$type=gettype($var);
		
	    $string = '<table>';
	    if ($title) {
	    	$time=date("Y-m-d  H:i:s ");
	    	$sender  = self::get_sender();
	    	$string .=	"<tr class=\"varheader\"><th class=\"header_expanded leftpadding\"><b>Varname</b></th><th style=\"background:none;\">$varname&nbsp;:&nbsp;type = <i>$type</i></th></tr>\n";
	    	$string .=	"<tr><th><b>Time</b></th><th>$time</th></tr>\n";
	    	$string .=	"<tr><th><b>Sender</b></th><th>$sender</th></tr>\n";
	    	$string .=	"<tr><th><b>Message</b></th><th>$message</th></tr>\n";
	        $string .= "<tr><th><b>Key</b></th><th><b>Value</b></th></tr>\n";
	    }
		
	    if (is_array($var)) {
	    	
	    	if(count($var)==0){
	    		$string .= "<tr>\n" ;
			    $string .= "<td >";
	    		$string .= '<i>array</i>&nbsp;:&nbsp;{0}' ;
	    		$string .= "</td></tr>\n";
	    		
	    	} else {
	    		
			      foreach($var as $key => $value) {
			          if(!strstr($key, "debug") && $key!='dumpload'){//filteren sessievars debugger
				          $string .= "<tr>\n" ;
				          $string .= "<td ".($title?'class="varname"':'')." style=\"cursor:pointer;\"><b>$key</b></td><td  id=\"tr_$key\">";
				
				          if (is_array($value)) {
				              $string .= self::printArray($value, false);
				          } elseif(gettype($value) == 'object') {
				          	
				              	$string .= "<i>Object of class " . get_class($value)."&nbsp;;&nbsp;public vars only</i>";
				              	$string.="<br>"; 	
				         		$string.=self::printObject($value,false);
				         		$string.="<br><i>All properties :</i><br>";
		    					$string.=self::vardump($var);
				          } else {
				          	  if(gettype($value)=='boolean'){
				          	  	$value==1?$value='true':$value='false';
				          	  	$string .= '<i>boolean</i>&nbsp;:&nbsp;'.$value ;
				          	  } else {
				          	  	if($value==NULL){
				          	  		$string .= '<i>NULL</i>' ;
				          	  	} else {
				              		$string .= '<i>'.gettype($value).'</i>&nbsp;:&nbsp;'.$value ;
				          	  	}
				          	  }
				          }
				
				          $string .= "</td></tr>\n";
			          }
			      }
	    	}
	    } else {
	    	
	    	$string .= "<tr>\n" ;
			$string .= "<td ".($title?'class="varname"':'')."  style=\"cursor:pointer;\"><b>$$varname</b></td><td >";
			
	    	switch($type) {
	    		case('object') :
	    			$string .= "<i>Object of class " . get_class($var)."&nbsp;;&nbsp;public vars only</i>";
	    			$string.="<br>";
	    			
	    			$string.=self::printObject($var,false);
	    			$string.="<br><i>All properties :</i><br>";
	    			$string.=self::vardump($var);
			
	    		break;
	    		case('boolean') :
	    			$var==1?$var='true':$var='false';
			        $string .= '<i>boolean</i>&nbsp;:&nbsp;'.$var ;
	    		break;
	    		case('NULL') :
	    			$string .= '<i>NULL</i>' ;
	    		break;
	    		default :
	    			
			        $string .= '<i>'.gettype($var).'</i>&nbsp;:&nbsp;'.$var ;	
	    		break;
	    		
	    	}
	    	$string .= "</td></tr>\n";
	    }
	
	    $string .= "</table>\n";
	    return $string;
  }
  
  
  
  
 public static function printObject($var,$title=true) {
		$type=gettype($var);
	    $string = '<table>';
	   
	    	if(count($var)==0){
	    		$string .= "<tr>\n" ;
			    $string .= "<td >";
	    		$string .= '<i>object</i>&nbsp;:&nbsp;{0}' ;
	    		$string .= "</td></tr>\n";
	    		
	    	} else {
	    		
			      foreach($var as $key => $value) {
			          
			          $string .= "<tr>\n" ;
			          $string .= "<td ".($title?'class="varname"':'')."><b>$key</b></td><td >";
			
			          if (is_array($value)) {
			              $string .= self::printArray($value, false);
			          } elseif(gettype($value) == 'object') {
			              $string .= self::printObject($value,false);
			          } else {
			          	  if(gettype($value)=='boolean'){
			          	  	$value==1?$value='true':$value='false';
			          	  	$string .= '<i>boolean</i>&nbsp;:&nbsp;'.$value ;
			          	  } else {
			          	  	if($value==NULL){
			          	  		$string .= '<i>NULL</i>' ;
			          	  	} else {
			              		$string .= '<i>'.gettype($value).'</i>&nbsp;:&nbsp;'.$value ;
			          	  	}
			          	  }
			          }
			
			          $string .= "</td></tr>\n";
			      }
	    	}
	    
	
	    $string .= "</table>\n";
	    return $string;
  }
  
  public static function vardump($var){
	ob_start();
	//echo '<pre>';var_dump($var);echo '</pre>';
    var_dump($var);
	$result = ob_get_contents();
	ob_end_clean();
	return $result;
	}
	
	public static function clearGarbage(){
		
		file_exists(DEBUGPATH)? $db=unlink(DEBUGPATH):$db=false;
		file_exists(FLAGPATH)? $fl=unlink(FLAGPATH):$fl=false;
		return $fl&$db;
	}
	
	public static function RemoveDir($dir, $DeleteMe) {

	    if(!$dh = @opendir($dir)) return;
	
		   while (false !== ($obj = readdir($dh))) {
	
		    	if($obj=='.' || $obj=='..') continue;
		    	if (!@unlink($dir.'/'.$obj)) {RemoveDir($dir.'/'.$obj, true);}
		    }
	
	    closedir($dh);
	
	    if ($DeleteMe){
	    	@rmdir($dir);// verwijder dir
	   	}
	}
	
	public static function get_debugID(){
		$backtrace = debug_backtrace();
		$file = $backtrace[1]['file'];
		if(strpos($file,'/')>-1){
			$arr_file = explode('/',$file);
		}else if(strpos($file,'\\')>-1){
			$arr_file = explode('\\',$file); 
		}else{
			return $file.session_id();
		}
		$count = count($arr_file);
		if($count==1){
			return $arr_file[$count-1].session_id();
		}else if($count>1){
			return $arr_file[$count-2].'-'.$arr_file[$count-1].session_id();
		}else{
			return $file.session_id();
		}
	}
    
    private static function get_sender(){
		$backtrace = debug_backtrace();
		$file = $backtrace[1]['file'];
        $line = $backtrace[1]['line'];
		
		return $file." line: ".$line;
		
	}
//RemoveDir('EmptyMe', false);
//RemoveDir('RemoveMe', true);

	
}
?>