<?php
include('../debug.php');
if(isset($_POST['clear'])){
	debug::Init('debuginit');
	
}
$w='';
$w.='
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Debug Dump</title>
<style type="text/css">
body {
	font-family:verdana;
	font-size:0.7em;
	color:black;
}
table {
	border-collapse:collapse;
	border: 3px solid #a0a0a0;
	margin:20px 0px 20px 0px;
}
table th {
	padding:5px;
	border:3px solid #a0a0a0;
	text-align:left;
	font-weight : normal;
	color : #0079B7;
}
table td {
	padding:5px;
	border: 1px solid #c0c0c0;
}

i {
	color:#0079B7;
}
hr {
	margin-top:7px;
	margin-left:0px;
	margin-bottom:10px;
	color:#0079B7;
	background-color:#0079B7;
	height:1px;
	border:none;
}
.btn {
	width:130px;
	margin-right:30px;
	border : 1px solid #0079B7;
	-moz-border-radius: 15px;
	border-radius: 15px;
	background-color: #F0F0F0;
	height:25px;
	cursor:pointer;
	background : transparent url(../img/btn-back.gif) left top repeat-x;
}
.btn:hover {
	background-color:#F8F8F8;
	text-decoration:underline;
}
pre {
	color:#0079B7;
	font-size:1.2em;
}
.toon {
	display:block;
	width:100%;
}
.verberg {
	display:none;
	width:100%;
}
.left{
	width:300px;
	
}
.right{
	width:700px;
}
.rowhide{
    display : none;
}
.row_collapsed{
    color : red!important;
}

.header_expanded {
	background : transparent url(../img/up.png) 3px 0px no-repeat;
}
.header_collapsed {
    background : transparent url(../img/down.png) 3px 0px no-repeat!important;
}
.leftpadding{
    padding-left:30px;
    width:150px;
}
.var_dump{
    width:100%;
    border:none;
    font-family:courier new,consolas;
    font-size:1.1em;
    min-height:1000px;
}
.vardump_wrap{
	border : 1px solid #0079B7;
	padding:5px;
}
</style>
    <script type="text/javascript"src="../js/utils/util.js"></script>
    <script type="text/javascript"src="../js/utils/request.js"></script>
    <script type="text/javascript"src="../js/utils/events.js"></script>
    <script type="text/javascript"src="../js/jquery/jquery.js"></script>
    <script type="text/javascript"src="../js/debug.js">
</script>
</head>
<body>
<p><h1>Debug-dump</h1></p>
<hr>
<div style="margin:20px auto 20px 100px;width:100%;height:40px">
<form method="post" action="index.php">
<input  class="btn" type="submit" name="clear" id="btn_clear" value="Clear Dump" />
<input  class="btn" type="button" name="reload" value="Reload Dump" onclick="location.replace(\''.HOST.'debug/dump/\')" /><input  class="btn" type="button" name="" id="btn_tail" value="Tail" />
<input  class="btn" type="button" name="info" id="btn_phpinfo"  value="PHP-info" />
<input  class="btn" type="button" name="" id="bnt_collapse" value="Collapse all" />
<input  class="btn" type="button" name="" id="btn_expand" value="Expand all" />

</form>
</div>
<hr>
<div style="margin:20px auto 20px auto;width:100%;" id="wrapper">
';
echo $w;

echo'

<script type="text/javascript">
	GL.debug_id="'.(isset($_SESSION['debug_id'])?$_SESSION['debug_id']:'null').'";
</script>

';
//toolbar met erase-button
if(file_exists(DEBUGPATH)){
	include(DEBUGPATH);
} else {
	debug::Init('FIRSTLOAD');
}

$w='';
$w.='
</div>
</body>

';
echo $w;
//footer
exit();
?>