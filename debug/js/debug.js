var debug = function(){
	
	
		
	var oRow ={},
        oTable={},
		snap=[],
		isPolling = false;
   
	var _handler = {
		
		process : function(event) {
						var t=event.target
						switch(event.type)
						{
						case 'click' :
							if(t.id == 'btn_expand') { 
								_expandAll();
								_snapShot(); 
								objEvent.preventDefault(event);
							}
                            if(t.id == 'bnt_collapse') { 
								_collapseAll();
								_snapShot(); 
								objEvent.preventDefault(event);
							}
							if(t.id == 'btn_phpinfo') {
							
								$.winOpen({
									'root'		: $.getLocation().root,
									'url'		: 'show_info.php',
									'options'	: 'menubar=1,resizable=1,scrollbars=1,width=800,height=980'
								});
								objEvent.preventDefault(event);
							}
							if(t.id == 'btn_tail'){
								if(!isPolling){
									$.startPolling();
									isPolling=true;
									$(t).css({
										'border':'2px solid red'
									});
								} else {
									$.stopPolling();
									isPolling=false;
									$(t).css({
										'border':'1px solid #0079B7'
									});
								}
							}
							
						case 'resize' :
							//if(t == self || t == window.document) { handler.handleResize_window(t);}
							break;
						}
					},
					
					
		handleResize_window : function(t) {

					}
	};
	
	var output = { //XHR-interface
			
				setResult : function(oResponse, sStatus) {
					var content;
					bRequestActive=false;
					if(oResponse.data != 'nochange' && oResponse.data != 'error' && sStatus=='success'){
						$("#wrapper").html(  oResponse.data  );
						debug.init();
					}
				}
			};
			
	var XHR = {
				
				//req 01 ----------------------------------------------
				addPollUpdater:function() {

					$.addPoll({
						type 			: "get",
						url 			: 'getdebugdata.php',
						onsuccess 		: this.pollSuccessResult,
						onfailure 		: this.pollFailureResult,
						scope			: o //callbackscope
				
					});
				},
				
				pollSuccessResult : function(oResponse) {
					output.setResult(oResponse,'success');
				},
			
				pollFailureResult : function(oResponse) {
					output.setResult(oResponse,'error');
				}
				
	};
    
    var _expandAll = function(){
        
            $('.varheader').each(function(index){
                var el = $(this);
                var rows = el.parent()[0].rows.length;
                $(el.children()[0]).removeClass('header_collapsed');
                var row = el.next();
                for(var t=0;t<rows;t++){
                    row.removeClass('rowhide');
                    row=row.next();
                }
            });      
    };
    
    var _collapseAll = function(){
        
            $('.varheader').each(function(index){
                var el = $(this);
                var rows = el.parent()[0].rows.length;
                $(el.children()[0]).addClass('header_collapsed');
                var row = el.next();
                for(var t=0;t<rows;t++){
                    row.addClass('rowhide');
                    row=row.next();
                }
            });   
    };
	
	var _showHideRow = function(elem){
	    $(elem).toggleClass('row_collapsed');
	    $(elem).next().toggleClass('rowhide');   
	};
    
    var _showHideTable = function(elem){
        var el = $(elem);
        var rows = el.parent()[0].rows.length;
		$(el.children()[0]).toggleClass('header_collapsed');
        var row = el.next();
        for(var t=0;t<rows;t++){
            row.toggleClass('rowhide');
            row=row.next();
        }   
	};
	
	var _snapShot = function(){
		 $('.varheader').each(function(index){
				if(index!='undefined' && snap[index]!='undefined'){
					var el = $(this);
					snap[index]=[];
					var rows = el.parent()[0].rows.length;
					snap[index][0]=el.children()[0].className; 
					var row = el.next();	
					for(var t=0;t<rows-1;t++){
						snap[index][t+1]=row.children()[0].className;
						row=row.next();
					}
				}
            });
			$.setCookie(GL.debug_id,$.safeObjToURI(snap));
	};
	
	var _restoreFromSnapShot = function(){
		if($.getCookie(GL.debug_id)!=null){
			 snap = $.JSONparse( $.base64decode($.getCookie(GL.debug_id)) );
			 if(snap.length>0){
				 $('.varheader').each(function(index){
						if(index!='undefined' && snap[index]!='undefined'){
							var el = $(this);
							var rows = el.parent()[0].rows.length;
							if($.in_str('header_collapsed',snap[index][0]||'')){
								_showHideTable(this);
							}
							var row = el.next();	
							for(var t=0;t<rows-1;t++){
								row.children()[0].className=snap[index][t+1];
								if($.in_str('row_collapsed',snap[index][t+1]||'')){
									row.children()[1].className='rowhide';
								}
								
								row=row.next();
							}
						}
						
				 });
			 }
		} else {
			_snapShot();
		}
	};

	//PUBLIC INTERFACE
	var o = {

		identifier : 'debugger',
		
		init : function(){
			$('body').click(_handler.process);
            
			this.oRow=$('.varname');
			this.oRow.bind('click', function(){
			    _showHideRow(this);
				_snapShot();
            });
            
            this.oTable=$('.varheader');
			this.oTable.bind('click', function(){
			    _showHideTable(this);
				_snapShot();
   
			});
			this.oRow.css({
			    'cursor'       : 'pointer',
			    'vertical-align'   : 'top'
			});
            
            this.oTable.css({
			    'cursor'       : 'pointer',
			    'vertical-align'   : 'top'    
			});
            
            this.oWrapper=$('table');
			
			_restoreFromSnapShot();

	   },
	   
	   setPolling:function(interval){
					$.setPollingInterval(interval);
					XHR.addPollUpdater();		
		}	
	};
	
	return o;

}();

//========= start ===========      
$(document).ready(function(){
	$.extend(Util);//tools object
	$.extend(RequestManager);//xhr object
	$.logInit();//logger
	debug.init();
	debug.setPolling(2000);
});
