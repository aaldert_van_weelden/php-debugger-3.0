var loggerWindow = (function(){
	
			//PRIVATE METHODS EN VARS
			var isPolling = false;
			var _handler = {
		
				process : function(event) {
					var t=event.target
						switch(event.type) {
							case 'click' :
								
								if(t.id == 'btn_tail'){
									if(!isPolling){
										$.startPolling();
										isPolling=true;
										$(t).css({
											'border':'2px solid red'
										});
									} else {
										$.stopPolling();
										isPolling=false;
										$(t).css({
											'border':'1px solid #0079B7'
										});
									}
								}
								
							case 'resize' :
								//if(t == self || t == window.document) { handler.handleResize_window(t);}
								break;
							}
					},
					
					
				handleResize_window : function(t) {

				}
			};
			
			var output = { //XHR-interface
			
				setResult : function(oResponse, sStatus) {
					var content;
					bRequestActive=false;
					if(oResponse.data != 'nochange' && oResponse.data != 'error' && sStatus=='success'){
						$("#cnt_display").html(  oResponse.data  );
					}
				}
			};
			
			var XHR = {
				
				//req 01 ----------------------------------------------
				addPollUpdater:function() {

					$.addPoll({
						type 			: "get",
						url 			: 'getloggerdata.php',
						onsuccess 		: this.pollSuccessResult,
						onfailure 		: this.pollFailureResult,
						scope			: o //callbackscope
				
					});
				},
				
				pollSuccessResult : function(oResponse) {
					output.setResult(oResponse,'success');
				},
			
				pollFailureResult : function(oResponse) {
					output.setResult(oResponse,'error');
				}
				
			};
			
			//PUBLIC INTERFACE-----------------------------------
			var o={
				identifier : 'loggerWindow',
				
				init : function(){
					$('body').click(_handler.process);
				},
				
				setPolling:function(interval){
					$.setPollingInterval(interval);
					XHR.addPollUpdater();
				}	
				
			};
			return o;
			//--------------------------------------------------
		})();
		
//START===========================================================

		$(document).ready(function(){
		
			$.extend(Util);//tools object
			$.extend(RequestManager);//xhr object
			
			loggerWindow.init();
			loggerWindow.setPolling(2000);

		});
		
//================================================================