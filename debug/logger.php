<?php
class Logger {

    const DEBUG = 'DEBUG';
    const INFO  = 'INFO';
    const WARN  = 'WARN';
    const ERROR = 'ERROR';
    
	private static $instance = null;
	public static $USER_GROUP_RIGHTS = 0770;
	private $fileHandle;
	private $clearFileHandle;

	/**
	 * Logger constructor
	 *
	 * @access private
	 */
	private function __construct() {

		if (!file_exists(LOCAL_STORAGE_DIR)) {
			mkdir(LOCAL_STORAGE_DIR, self::$USER_GROUP_RIGHTS);
		}

		$this->fileHandle = fopen(LOCAL_STORAGE_DIR . LOG_FILE_NAME, "at+");
		
		if ($this->fileHandle === false) {
			print "Failed to obtain a handle to log file '" . LOG_FILE_NAME . "'";
		}

	}

	/**
	 * returns an instance of the Logger object
	 *
	 * @access public
	 * @static
	 *
	 * @return an instance of the Logger object
	 */
	public static function getInstance() {
		if (Logger::$instance == null) {
			Logger::$instance = new Logger();
		}
		return Logger::$instance;
	}

	public function debug($textMessage) {	
		$textMessage = $this->formatMessage($textMessage,self::DEBUG);
	}
    
    public function info($textMessage) {
		$textMessage = $this->formatMessage($textMessage, self::INFO);
	}
    
    public function warn($textMessage) {
		$textMessage = $this->formatMessage($textMessage, self::WARN);
	}
    
    public function error($textMessage) {
		$textMessage = $this->formatMessage($textMessage, self::ERROR);
	}
	
	/**
	 * closes the handle to the log file
	 *
	 * @access public
	 */
	public function close() {		
		$success = fclose($this->fileHandle);
		if ($success === false) {
			// Failure to close the log file
			$this->write("Logger failed to close the handle to the log file", ERROR_SEVERITY);
		}
			
		Logger::$instance = 0;	
	}

    /*
    //TODO fix this
    public function clear($msg=''){
        if ($this->clearFileHandle !== false) {
			
			if (fwrite($this->clearFileHandle, $msg) === false) {
				print "There was an error clearing the log file.";
                return false;
			}
		}
    }
    */
   
	/**
	 * formats the error message in representable manner
	 *
	 * @param message this is the message to be formatted
	 *
	 * @return the formatted message
	 */
	private function formatMessage($message, $severity) {
        
        switch($severity){
            case self::WARN:
                $colorFormat = '<span style="color:orange;">';
            break;
            case self::ERROR:
                $colorFormat = '<span style="color:red;">';
            break;
            default:
                $colorFormat = '<span style="color:#fff;">';
            break;
        }
		$msg = date("G:i:s") . " "; 
		$msg .= $_SERVER['REMOTE_ADDR'];
        $msg .= $this->calculateSpaces(15,$_SERVER['REMOTE_ADDR'],false);
        
		$msg .= $colorFormat;
		$msg .= " ".$severity;
        $msg .= $this->calculateSpaces(6,$severity);
        
        $sender = $this->get_sender();
        $msg .= $sender;
        $msg .= $this->calculateSpaces(50,$sender);
        
		$msg .=  $message;
        $msg .= '</span><br>';
 
        if ($this->fileHandle !== false) {
			
			if (fwrite($this->fileHandle, $msg) === false) {
				print "There was an error writing to log file.";
                return false;
			}
		}
        return true;
	}
    
    private function calculateSpaces($len,$str,$colon=true){
        $numWhitespaces =  $len - strlen($str);
        $sp = '';
		for ($i=0; $i<$numWhitespaces; $i++) {
			$sp .= "&nbsp;";
		}
        if($colon){
            $sp .= ":&nbsp;";
        }
        return $sp;
    }
	
	private function get_sender(){
		$backtrace = debug_backtrace();
		if($backtrace[3]!=null && is_array($backtrace[3])){
			$file = $backtrace[3]['file'];
			$line = $backtrace[3]['line'];
			if(strpos($file,'/')>-1){
				$arr_file = explode('/',$file);
			}else if(strpos($file,'\\')>-1){
				$arr_file = explode('\\',$file);
			}else{
				return $file." line ".$line;
			}
			$count = count($arr_file);
			if($count>1){
				return $arr_file[$count-2].'/'. $arr_file[$count-1]." line ".$line;
			}else if($count==1){
				return $arr_file[0]." line ".$line;
			}else{
				return $file." line ".$line;
			}
			
			return $file." line ".$line;
		}
		return '';
	}
	
	
}


?>