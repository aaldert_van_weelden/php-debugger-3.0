<?php
//START SESSIE
if (!isset($_SESSION)) session_start();

include('config.php');
include('debug.php');

$aFlagsHTML=array();
$aFlagsTXT=array();
$IPok='';
$error='';
$debug = null;
$test = null;
$hold = null;
$logdebug = null;
$loginfo = null;
$logwarn = null;
$logerror = null;

//flags in session zetten
foreach($aFlags as $name){
	

	if(isset($_POST[$name])) {
		$_SESSION['debug_'.$name]=true;
	}
	
	if(isset($_POST['no'.$name])) {
		$_SESSION['debug_'.$name]=false;
	}
	
}



//wanneer ingelogd :
if( isset($_SESSION['debug']) && $_SESSION['debug'] == 'open'){
	
	debug::makeCode();//
}
//VERZENDEN LOGINFORMULIER
if(isset($_POST['login'])) {
	if(!empty($_POST['user']) && !empty($_POST['pass'])){
		
		$usernaam=trim( strtolower($_POST['user']) );
		$password=trim( $_POST['pass']) ;
		
		if($a_debugger[$usernaam] == $password) {
			
			
			$_SESSION['debug'] = 'open';
			$_SESSION['debug_user']=$usernaam;
			header("Location: ".HOST."debug/");
		} else {
			$error = 'onjuiste gebruikersnaam en/of wachtwoord';
		}
	} else {
		$error = 'Vul een gebruikers naam en /of wachtwoord in ';
	}
}

//UITLOGGEN
if(isset($_POST['logout'])) {
	unset($_SESSION['debug']);
	unset($_SESSION['debug_user']);
	foreach($aFlags as $name){
		unset($_SESSION['debug_'.$name]);
	}
	debug::clearGarbage();
	header("Location: ".HOST."debug/");
}


//knoppen opbouwen
if(isset($_SESSION['debug']) && $_SESSION['debug'] == 'open'){
	
   
	if(in_array($_SERVER['REMOTE_ADDR'],$a_IP)){
		$IPok='<i style="color:green">IP-adres toegestaan</i>';
	}else{
		$IPok='<i style="color:red;">IP-adres verboden, pas de configuratie aan</i>';
		$error = 'IP-adres verboden';
	}
	
	//FLAG-KNOPPPEN GENEREREN
	foreach($aFlags as $name){
		$aFlagsHTML[$name]['on']='<div><input class="aan" name="'.$name.'" id="'.$name.'" type="submit" value="'.strtoupper($name).' aanzetten" style="width: 150px;" /></div>';
		$aFlagsHTML[$name]['off']='<div><input class="uit" name="no'.$name.'" id="no'.$name.'" type="submit" value="'.strtoupper($name).' uitzetten" style="width: 150px;" /></div>';
	}
	
	$button='<input class="btn" name="logout" id="logout" type="submit" value="uitloggen" style="width: 150px;" />';
	$input='<br><b>U bent ingelogd als '.$_SESSION['debug_user'].'.</b>';
	$input.='<br>IP-adres : <b>'.$_SERVER['REMOTE_ADDR'];
	$input.=' --> '.$IPok;
	$input.='</b><br><br>';
	$input.='<i>Gedefinieerde DIR : '.HOST.'debug/</i><br><br>';
	$input.='<span><a href="'.HOST.'debug/dump/" target="_blank"><ol>New Dump</ol></a></span>';
    $input.='<span><a href="'.HOST.'debug/logs/" target="_blank"><ol>New Logger</ol></a></span>';
	
	foreach($aFlags as $name){
		if(isset($_SESSION['debug_'.$name]) && $_SESSION['debug_'.$name]){
			$aFlagsTXT[$name]='<span class="signon">&nbsp;&nbsp;&nbsp;&nbsp;</span><b style="color:red;">'.strtoupper($name);
		}else{
			$aFlagsTXT[$name]='<span class="signoff">&nbsp;&nbsp;&nbsp;&nbsp;</span><b>'.strtoupper($name);
		}

	}
	
} else {
	$button='<div><input class="btn" name="login" id="login" type="submit" value="inloggen" style="width: 150px;" /></div><br>';
	$input='<div><input style="width:200px" name="user" id="user" type="text" />&nbsp;Gebruikersnaam</div><br>
	<div><input style="width:200px" name="pass" id="pass" type="password" />&nbsp;Wachtwoord</div><br><br><br><br>';
}


//opbouw formulier
 $w='';
 $w.='

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login Testomgeving</title>
<style type="text/css">
body {
	font-family:verdana;
	font-size:12px;
	color:black;
}
i {
	color:blue;
}
input[type="submit"] {
	margin-bottom:15px;
	border:none;
	width:154px;
	height:24px;
}
a ol {
	padding:4px 32px 5px 32px;
	text-decoration:none;
	background-image:url(img/btn.png);
	color:black;
	width:85px;
}
a ol:hover {
	background-image:url(img/btn_over.png);
}
.aan {
	background-image:url(img/btn.png);
}
.uit {
	background-image:url(img/btn_aan.png);
}
.btn {
	background-image:url(img/btn.png);
}

.signon {
	background-color:red;
	border:1px solid grey;
	margin-right:10px;
    border-radius:10px;
}
.signoff {
	background-color:white;
	border:1px solid grey;
	margin-right:10px;
    border-radius:10px;
}

</style>
</head>
<body onLoad="document.getElementById(\'user\')?document.getElementById(\'user\').focus():void(0);">


<form method="POST"  style="text-align: left; width: 500px;margin:10px auto 0px auto;">
 		<p><h1>Debug-omgeving</h1></p>';

if(!empty($error)){
	$w .= '<p style="text-align: center; width: 100%;color: black; font-weight: bold;font-size:2em;background-color:red;border-radius:10px;padding:5px;">'.$error.'</p>';
}		
		
$w .= $input;
$w .= $button;
 
if(isset($_SESSION['debug']) && $_SESSION['debug'] == 'open'){
 
	$w.='<p style="text-align: left; width:500px;color: blue; font-weight: bold;padding:20px;border:1px solid blue;margin:10px auto 0px auto;">
	<span style="color:black;font-size:1.1em;">TestFlags :</span> <br><br>';
	 
	 foreach ($aFlags as $name){
	 	if(isset($aFlagsTXT[$name])) $w.=$aFlagsTXT[$name].'<br><br>';
	 }
	 
	 $w.='</p><br><br>';
	 
}
 foreach($aFlags as $name){
 	if(isset($_SESSION['debug_'.$name]) && $_SESSION['debug_'.$name]){
 		if(isset($aFlagsHTML[$name])) $w.=$aFlagsHTML[$name]['off'];
 	} else {
 		if(isset($aFlagsHTML[$name])) $w.=$aFlagsHTML[$name]['on'];
 	}
 }
 
$w.='
</form>
</body>
';
 
 echo $w;
?>