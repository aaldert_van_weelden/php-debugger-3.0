<?php
//read from file
session_start();
function getPath ($p_iIndex=0, $p_iSelf=false){
		
		$s_path='';
		$a_path=explode("/", $_SERVER['PHP_SELF']);
		
		if($p_iIndex>count($a_path)) {
			$p_iIndex=count($a_path);	
		}
		
		for($t=0;$t<=$p_iIndex;$t++) {
			$t<$p_iIndex?$slash='/':$slash='';
			$s_path .= $a_path[$t].$slash;
	
		}
		
		if($p_iSelf){
			$path=$_SERVER['HTTP_HOST'].$s_path.'/';
			$path=str_replace("//", "/", $path);
			$path='http://'.$path;
		}
		else{
			$path=$_SERVER['DOCUMENT_ROOT'].$s_path.'/';
			$path=str_replace("//", "/", $path);
		}

	
	return $path;
	}
    

define('LOGFILE_PATH', getPath(0).'debug/temp/log.txt');

$cache = null;
if(isset($_SESSION['debugdata'])){
	$cache = $_SESSION['debugdata'];
}

if(file_exists(LOGFILE_PATH)){

    $content = file_get_contents(LOGFILE_PATH);
	$hash = md5($content);
	
	if($hash==$cache){
		//no changes
		echo 'nochange';
	}else {
		//changes
		$_SESSION['debugdata']=$hash;
		echo $content;
	}

} else {
	echo 'error';
}
?>