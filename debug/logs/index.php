<?php
include('../debug.php');
if(isset($_POST['clear'])){
	//clear logfile here
    debug::InitLogger('logger init');
	
}
$w='';
$w.='
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Debug Logging</title>
<style type="text/css">
body {
	font-family:verdana;
	font-size:0.7em;
	color:black;
}
table {
	border-collapse:collapse;
	border: 3px solid #a0a0a0;
	margin:20px 0px 20px 0px;
}
table th {
	padding:5px;
	border:3px solid #a0a0a0;
	text-align:left;
	font-weight : normal;
	color : blue;
}
table td {
	padding:5px;
	border: 1px solid #c0c0c0;
}

i {
	color:blue;
}
hr {
	margin-top:7px;
	margin-left:0px;
	margin-bottom:10px;
	color:#0079B7;
	background-color:#0079B7;
	height:1px;
	border:none;
}
.btn {
	width:130px;
	margin-right:30px;
	border : 1px solid #0079B7;
	-moz-border-radius: 15px;
	border-radius: 15px;
	background-color: #F0F0F0;
	height:25px;
	cursor:pointer;
	background : transparent url(../img/btn-back.gif) left top repeat-x;
}
.btn:hover {
	background-color:#F8F8F8;
	text-decoration:underline;
}
pre {
	color:blue;
	
}
.toon {
	display:block;
	width:100%;
}
.verberg {
	display:none;
	width:100%;
}
.left{
	width:300px;
	
}
.right{
	width:700px;
}
.rowhide{
    display : none;
}
.rowred{
    color : red;
}
.display {
    margin:20px auto 20px auto;
    width:99%;
    height:740px;
    overflow:auto;
    font-family: consolas,courier new;
    font-size:14px;
    border:none;
    color: #fff;
    background-color:#000;
    padding:10px;
}
</style>
    <script type="text/javascript"src="../js/utils/util.js"></script>
    <script type="text/javascript"src="../js/utils/request.js"></script>
    <script type="text/javascript"src="../js/utils/events.js"></script>
    <script type="text/javascript"src="../js/jquery/jquery.js"></script>
    <script type="text/javascript"src="../js/logger.js"></script>
    

</head>
<body>
<p><h1>Debug-logger</h1></p>
<hr>
<div style="margin:10px auto 10px auto;width:700px;height:24px">
<form method="post" action="index.php">
<input  class="btn" type="submit" name="clear" value="Clear Logging" />
<input  class="btn" type="button" name="reload" value="Reload Logging" onclick="location.replace(\''.HOST.'debug/logs/\')" />
<input  class="btn" type="button" name="clear" id="btn_tail" value="Tail" />

</form>
</div>
<hr>
<div id="cnt_display" class="display">
';
echo $w;


$content = file_get_contents(LOCAL_STORAGE_DIR.LOG_FILE_NAME);
echo $content;

$w='';
$w.='
</div><!-- end cnt_display -->
<hr>
</body>

';
echo $w;
//footer
exit();
?>