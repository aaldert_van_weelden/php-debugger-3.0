<?php
include('debug/debug.php');//DEBUGGER INCLUDEN ALS STATIC HELPERCLASS
$w='';
$w.='
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Debug TestPage_1</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<link href="style.css" rel="stylesheet" type="text/css" />

<style type="text/css">

hr {
	margin-top:7px;
	margin-left:0px;
	margin-bottom:10px;
	color:blue;
	background-color:blue;
	height:1px;
	border:none;
}
pre {
	color:blue;
	
}
pre em{
	color:green;
	font-style:normal!important;
}
</style>

</head>
<body>


<div id="wrapper">
<div id="content">
<div id="left" class="twocols"></div><!--end left-->
<div id="center"  class="twocols">
	<form method="post" action="xhr/formactie.php" id="form">
		<fieldset>
		<legend>Testpagina 01 PHP Debugger</legend>
		<p>
		Deze debugger is eenvoudig te gebruiken en geschikt voor online werk omdat er een login functionaliteit is ingebouwd en een IP-adres check.
		</p>
		<ul>
		<li><a class="" href="http://code.google.com/p/php-debugger/downloads/list" target="_blank">GoogleCode</a></li>
        </ul>
<hr>
<a href="index02.php" target="_blank">Load Tweede testpagina</a>
<hr>
<div style="margin:20px auto 20px auto;width:100%;">
';
echo $w;

//test


    

class MyClass {
    public $var1 = 'value 1';
    public $var2 = 'value 2';
    public $var3 = 'value 3';
    public $var4 = array(1,2,3,4,5);


    protected $protected = 'protected aaldert';
    private   $private   = 'private aaldert';

    function iterateVisible() {
       echo "MyClass::iterateVisible:\n";
       foreach($this as $key => $value) {
           print "$key => $value\n";
       }
    }
}

$class = new MyClass();

 

$test=false;
$naam='aaldert van weelden';
$timestamp=time();
$leeg=NULL;
$regel=45;

$test_array = array('$test'=>$test,'$naam'=>$naam,'$timestamp'=>$timestamp, '$var'=>23,'$array'=>array(1,3,5,7,9));
//voorbeelden
debug::I();//init



debug::D(array('$test'=>$test,'$naam'=>$naam,'$regel'=>$regel, '$leeg'=>$leeg,'$class'=>$class),'array', 'array van testvariabelen');
debug::D($leeg,'leeg');
debug::D($class,'class');

debug::SES(__FILE__.__LINE__);//session 

debug::REQ(__FILE__.__LINE__);//request vars
debug::SER(__FILE__.__LINE__);//server
debug::GLB(__FILE__.__LINE__);//globals

debug::log('initial debug message, shows only if LOGDEBUG is true');


if(TEST){
	$message="<h1 style=\"color:grey;border:1px solid grey;padding:4px;\">&nbsp;Testmodus ; ingelogd</h1>";
    debug::info('testmode is on');
} else {
	$message="";
    debug::info('testmode is off');
}

debug::warn('watch out, this is a warning message. Shows only if LOGWARN is true');
debug::error('an error message for you, shows only if LOGERROR is true');

$w='';
$w.='
<p>Deze pagina bevat php-code die een aantal variabelen, arrays en een object debugt</p>
<p>Ga naar <a href="debug/" target="_blank">debug</a>  om in te loggen, [user:demo en password:demo]<br> 
schakel daarna de flags [DEBUG] , [TEST] en [LOG****] buttons in en reload de Debug Testpagina 1 of 2.<br><br>
Open dan <a href="debug/dump/" target="_blank">debug/dump</a> in een ander venster om de dump-resultaten te bekijken<br>
of open de logging pagina <a href="debug/logs/" target="_blank">debug/logs</a> om de logging te bekijken</p>
<br><br>
'.$message.'
<br><br>

<hr>
<p>
De code die de testobjecten genereert :
</p>
<pre class="indent">



class MyClass {
    public $var1 = &#39;value 1&#39;;
    public $var2 = &#39;value 2&#39;;
    public $var3 = &#39;value 3&#39;;
    public $var4 = array(1,2,3,4,5);


    protected $protected = &#39;protected aaldert&#39;;
    private   $private   = &#39;private aaldert&#39;;

    function iterateVisible() {
       echo &quot;MyClass::iterateVisible:\n&quot;;
       foreach($this as $key =&gt; $value) {
           print &quot;$key =&gt; $value\n&quot;;

       }
    }
}

$class = new MyClass();




$test=true;
$naam=&#39;aaldert&#39;;
$timestamp=time();

$leeg=NULL;

$test_array = array(&#39;$test&#39;=&gt;$test,&#39;$naam&#39;=&gt;$naam,&#39;$timestamp&#39;=&gt;$timestamp, &#39;$var&#39;=&gt;23,&#39;$array&#39;=&gt;array(1,3,5,7,9));








</pre>
<p>
De debugger-code :
</p>
<pre class="indent">

<em>
debug::I();//init

debug::V($naam,&#39;$naam&#39;);
debug::V($test_array,&#39;een test array&#39;);

debug::D(array(&#39;$test&#39;=&gt;$test,&#39;$naam&#39;=&gt;$naam,&#39;$timestamp&#39;=&gt;$timestamp,<br>
 &#39;$leeg&#39;=&gt;$leeg,&#39;$class&#39;=&gt;$class),&#39;array&#39;, &#39;array van testvariabelen&#39;);


debug::D($leeg,&#39;leeg&#39;);
debug::D($class,&#39;class&#39;);

debug::SES(__FILE__.__LINE__);//session 



debug::REQ(__FILE__.__LINE__);//request vars
debug::SER(__FILE__.__LINE__);//server
debug::GLB(__FILE__.__LINE__);//globals


debug::log(&#39;initial message&#39;);
</em>

if(TEST){
	$message=&quot;&lt;h1 style=\&quot;color:grey;border:1px solid grey;padding:4px;\&quot;&gt;&amp;nbsp;Testmodus ; ingelogd&lt;/h1&gt;&quot;;
    <em>debug::info(&#39;testmode is on&#39;);</em>
} else {
	$message=&quot;&quot;;
    <em>debug::info(&#39;testmode is off&#39;);</em>
}
<em>
debug::warn(&#39;watch out, this is a warning message. Shows only if LOGWARN is true&#39;);
debug::error(&#39;an error message for you, shows only if LOGERROR is true&#39;);</em>

</pre>
</div><br><br>
<p>
Meer documentatie is te vinden in de download op <a class="" href="http://code.google.com/p/php-debugger/downloads/list" target="_blank">GoogleCode</a>
</p>
</fieldset>
</form>

</div><!--end center-->
	<div id="right">';
	
	
	
	
	$w.='
	</div><!--end right-->
	<div class="clear"></div>
    </div><!--end content-->
</div><!--end wrapper-->

</body>

';
echo $w;
//footer
exit();
?>